%undefine __cmake_in_source_build
%global framework akonadi-notes

# uncomment to enable bootstrap mode
%global bootstrap 1
%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf5-%{framework}
Version:        23.08.5
Release:        2
Summary:        The Akonadi Notes Library
License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}
Source0:        http://download.kde.org/stable/release-service/%{version}/src/%{framework}-%{version}.tar.xz

%global majmin %majmin_ver_kf5

BuildRequires:  make
BuildRequires:  cyrus-sasl-devel
BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-rpm-macros
%global kf5_ver 5.23
BuildRequires:  kf5-ki18n-devel >= %{kf5_ver}
BuildRequires:  qt5-qtbase-devel

# global majmin_ver %(echo %{version} | cut -d. -f1,2)
%global majmin_ver %{version}
BuildRequires:  kf5-akonadi-server-devel >= %{majmin_ver}
BuildRequires:  kf5-kmime-devel >= %{majmin_ver}

%if 0%{?tests}
BuildRequires:  kf5-akonadi-server >= %{majmin_ver}
BuildRequires:  kf5-akonadi-server-mysql
BuildRequires:  xorg-x11-server-Xvfb
%endif

# split from kf5-akonadi/kdepimlibs in 16.07
Obsoletes:      kf5-akonadi < 16.07

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
# split from kf5-akonadi/kdepimlibs in 16.07
Obsoletes:      kf5-akonadi-devel < 16.07
Requires:       cmake(KPim5Mime)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
DBUS_SESSION_BUS_ADDRESS=
xvfb-run -a \
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_libdir}/libKPim5AkonadiNotes.so.*

%files devel
%{_kf5_libdir}/cmake/KF5AkonadiNotes/
%{_kf5_libdir}/cmake/KPim5AkonadiNotes/
%{_kf5_libdir}/libKPim5AkonadiNotes.so
%{_kf5_archdatadir}/mkspecs/modules/qt_AkonadiNotes.pri
%{_includedir}/KPim5/AkonadiNotes

%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Wed Jan 17 2024 misaka00251 <liuxin@iscas.ac.cn> - 23.08.4-1
- Upgrade to 23.08.4

* Fri Aug 04 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 23.04.3-1
- Update package to version 23.04.3

* Tue Apr 18 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 22.12.0-1
- Package init
